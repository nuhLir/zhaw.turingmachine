NI.Turing.Machine = function(){
	return {
		outputHandler: function(){},
		stateChangeHandler: null,
		track: null, 
		states: [],
		startStateIdentifier: null,

		/**
		 * Prints debug message into output
		 *
		 * @param {String} object
		 * @param {String} message
		*/
		out: function(object, message){
			var handler = this.getOutputHandler();

			handler(object, message);
		},

		/**
		 * Starts the execution of the turing machine
		*/
		run: function(){
			this.out('NI.Turing.Machine.start', 'Start execution');
			this.out('NI.Turing.Machine.start', this.getTrack());

			var states = this.getStates(),
				track = this.getTrack(),
				stepsCount = 0,
				currentStateIdentifier = this.startStateIdentifier,
				currentTrackPosition = 0,
				currentTrackObject = null,
				currentState = states[this.startStateIdentifier];

			// enter the turing machine execution loop
			// and do it until all the states on the path were executed
			while (currentState !== null){
                if (currentTrackPosition >= track.length){
					currentTrackObject = NI.Turing.EMPTY.toString();
				} else {
					currentTrackObject = track[currentTrackPosition];

					if (typeof currentTrackObject === 'undefined'){
						currentTrackObject = NI.Turing.EMPTY;
					}

					currentTrackObject = currentTrackObject.toString();
				}

				// execute the current state
				var result = currentState(currentTrackObject);

				// no outputs were specified, so this must be a target state
				if (Object.keys(result).length === 0){
					this.out('NI.Turing.Machine.Execute', 'Reached last state, quit machine execution');

					currentState = null;
					continue;
				}

				// loop through the output channels
				// and continue the stuff
				for (var i = 0; i < result.length; i++){
					var outputChannel = result[i];

					// check if target state was specified
					// if yes, go to it
					if (outputChannel.hasOwnProperty('target')){
						var conditionFullFilled;

						// if condition is a handler, we execute it to check if was fullfilled
						if (typeof outputChannel.condition === 'function'){
							conditionFullFilled = outputChannel.condition();
						} else {
							// otherwhise we just compare the current track value with the one
							// specified in the channel output
							conditionFullFilled = currentTrackObject === outputChannel.condition.toString();
						}

						// check if condition was fullfilled
						if (conditionFullFilled){
							currentState = states[outputChannel.target];

							this.out('NI.Turing.Machine.Execute', 'Move to new state '+outputChannel.target);

							// write result into current band and continue
							if (outputChannel.hasOwnProperty('write')){
								this.writeTrackSlot(currentTrackPosition, outputChannel.write);
							}

							// shift the track if direction was specified
							if (outputChannel.hasOwnProperty('direction')){
								currentTrackPosition += outputChannel.direction;
							}

							stepsCount ++;

							if (this.stateChangeHandler !== null){
								this.stateChangeHandler.fnc.call(this.stateChangeHandler.scope, currentStateIdentifier, outputChannel.target, track, currentTrackPosition, stepsCount);
							}

							currentStateIdentifier = outputChannel.target;

							break;
						}
					} else {
						console.error('NI.Turing.Machine.Execute(): State output channel has no target state specified!', currentState);
					}
				}
			}
		},

		/**
		 * Write track slot
		 *
		 * @param {Number} trackPosition
		 * @param {Number} value
		*/
		writeTrackSlot: function(trackPosition, value){
			var track = this.getTrack();

			if (trackPosition !== -1) track[trackPosition] = value;
		},

		/**
		 * Adds the states
		 *
		 * @param {String} stateName
		 * @param {Function} handler
		*/
		addState: function(stateName, handler){
			this.states[stateName] = handler;
		},

		/**
		 * Set states in declarative manner
		 *
		 * @param {Array} states
		*/
		setStates: function(states){
			this.states = states;
		},

		/**
		 * Returns the States
		*/
		getStates: function(){
			return this.states;
		},

		/**
		 * Sets the track which is executed in this Turing machine
		 *
		 * @param {Array} track
		*/
		setTrack: function(track){
			this.track = track;
		},

		/**
		 * Gets the track list
		 *
		 * @returns {Array} track
		*/
		getTrack: function(){
			return this.track;
		},

		/**
		 * Sets the output handler
		 *
		 * @param {Function} outputHandler
		*/
		setOutputHandler: function(outputHandler){
			this.outputHandler = outputHandler;
		},

		/**
		 * Sets state change handler
		 *
		 * @param {Function} handler
		*/
		setStateChangeHandler: function(handler){
			this.stateChangeHandler = handler;
		},

		/**
		 * Gets the output handler
		 *
		 * @returns {Function} outputHandler
		*/
		getOutputHandler: function(){
			return this.outputHandler;
		},

		/**
		 * Set start state
		 *
		 * @param {String} startStateIdentifier
		*/
		setStartState: function(startStateIdentifier){
			this.startStateIdentifier = startStateIdentifier;
		}
	};
};