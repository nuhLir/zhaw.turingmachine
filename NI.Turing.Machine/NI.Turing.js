var NI = {
	Turing: {
		Machine: function(){},
		Track: function(){},
		State: function(){},

		EMPTY: -1,

		Directions: {
			LEFT: -1,
			RIGHT: 1
		},

		ExecutionModes: {
			IMMEDIATE: 0,
			STEP: 1
		}
	}
};