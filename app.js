NI.Application = function(){
	return {
        machineData: [],
        turingMachine: null,
        myTrack: [],
		initialize: function(){
			this.turingMachine = new NI.Turing.Machine();
		},

		setOutputHandler: function(outputHandler){
			this.turingMachine.setOutputHandler(outputHandler);
		},

		/**
		 * Prepares the turing machine with all states and the track
		*/
		prepareMachine: function(){
			this.turingMachine.setStates({
				"q0": function(input){
					return [
						{
							condition: 'x',
							direction: NI.Turing.Directions.LEFT,
							write: 'x',
							target: 'q10'
						},
						{
							condition: '1',
							direction: NI.Turing.Directions.RIGHT,
							write: 'A',
							target: 'q1'
						}
					];
				},

				"q1": function(input){
					return [
						{
							condition: '1',
							direction: NI.Turing.Directions.RIGHT,
							write: '1',
							target: 'q1'
						},
						{
							condition: 'x',
							direction: NI.Turing.Directions.RIGHT,
							write: 'x',
							target: 'q2'
						}
					];				
				},

				"q2": function(){
					return [
						{
							condition: '=',
							write: '=',
							direction: NI.Turing.Directions.LEFT,
							target: 'q3'
						},
						{
							condition: '1',
							write: 'B',
							direction: NI.Turing.Directions.RIGHT,
							target: 'q4'
						}
					];
				},

				"q4": function(){
					return [
						{
							condition: '=',
							write: '=',
							direction: NI.Turing.Directions.RIGHT,
							target: 'q5'
						},
						{
							condition: '1',
							write: '1',
							direction: NI.Turing.Directions.RIGHT,
							target: 'q4'
						}
					];
				},

				"q5": function(){
					return [
						{
							condition: '1',
							write: '1',
							direction: NI.Turing.Directions.RIGHT,
							target: 'q5'
						},
						{
							condition: NI.Turing.EMPTY,
							write: '1',
							direction: NI.Turing.Directions.LEFT,
							target: 'q6'
						}
					];
				},

				"q6": function(){
					return [
						{
							condition: '1',
							write: '1',
							direction: NI.Turing.Directions.LEFT,
							target: 'q6'
						},
						{
							condition: '=',
							write: '=',
							direction: NI.Turing.Directions.LEFT,
							target: 'q7'
						}
					];
				},

				"q7": function(){
					return [
						{
							condition: '1',
							write: '1',
							direction: NI.Turing.Directions.LEFT,
							target: 'q7'
						},
						{
							condition: 'B',
							write: 'B',
							direction: NI.Turing.Directions.RIGHT,
							target: 'q8'
						}
					];
				},

				"q8": function(){
					return [
						{
							condition: '1',
							write: 'B',
							direction: NI.Turing.Directions.RIGHT,
							target: 'q4'
						},
						{
							condition: '=',
							write: '=',
							direction: NI.Turing.Directions.LEFT,
							target: 'q3'
						}
					];
				},

				"q3": function(){
					return [
						{
							condition: 'B',
							write: '1',
							direction: NI.Turing.Directions.LEFT,
							target: 'q3'
						},
						{
							condition: 'x',
							write: 'x',
							direction: NI.Turing.Directions.LEFT,
							target: 'q9'
						}
					];
				},

				"q9": function(){
					return [
						{
							condition: '1',
							write: '1',
							direction: NI.Turing.Directions.LEFT,
							target: 'q9'
						},
						{
							condition: 'A',
							write: 'A',
							direction: NI.Turing.Directions.RIGHT,
							target: 'q0'
						}
					];
				},

				"q10": function(){
					return [
						{
							condition: 'A',
							write: '1',
							direction: NI.Turing.Directions.LEFT,
							target: 'q10'
						},
						{
							condition: NI.Turing.EMPTY,
							write: NI.Turing.EMPTY,
							direction: NI.Turing.Directions.RIGHT,
							target: 'q11'
						}
					];
				},

				"q11": function(){
					return [];
				}

			});
		},

		/**
		 * Prepares the track for multiplication operation
		 *
		 * @param {Number} x
		 * @param {Number} y
         * @returns {Array} the prepared track
		*/
		prepareTrackForMultiplication: function(x, y){
			var track = [];

            x.split("").forEach(function(){
                track.push('1');
            });

			track.push('x');

            y.split("").forEach(function(){
                track.push('1');
            });

			track.push('=');

			this.turingMachine.setTrack(track);
            return track;
		},

		/**
		 * Start executing the machine
		*/
		execute: function(){
			this.prepareMachine();

		//	this.turingMachine.setStepMode(NI.Turing.ExecutionModes.STEP);
			this.turingMachine.setStartState('q0');
			this.turingMachine.setStateChangeHandler({
				scope: this,
				fnc: this.machineStateChanged
			});

			this.turingMachine.run();

			console.log( this.turingMachine.getTrack() );
			console.log( this.extractResultFromTrack() );
		},

		/**
		 * Machine state has changed
         *
		*/
		machineStateChanged: function(currentState, newState, track, trackPosition, stepCount){
            this.machineData.push([currentState, newState, track.slice(0), trackPosition, stepCount]);
		},

        /**
         * Extracts the machine data, to be used in frontend
         *
         * @returns {Array} the collection of machine data
         */
        extractMachineData: function(){
            return this.machineData;
        },

		/**
		 * Extracts the result from track
		 *
		 * @return {Number} the result from the turing track
		*/
		extractResultFromTrack: function(){
			var result = 0, startMeasuring = false;

			for (var i = 0; i < this.turingMachine.getTrack().length; i++){
				var trackItem = this.turingMachine.getTrack()[i];

				if (trackItem === "1" && startMeasuring) result++;
				if (trackItem === "=") startMeasuring = true;
			}

			return result;
		}

	};
};