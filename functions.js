/**
 * Runs the machine
 * @param mode {number} 0 = automatic, 1 = slow motion, 2 = step by step
 */
function runMachine(mode){
    var app = new NI.Application();
    var factor1 = document.getElementsByTagName("input")[0].value;
    var factor2 = document.getElementsByTagName("input")[1].value;
    var outputArea = document.getElementsByClassName("output")[0];

    app.initialize();

    /*app.setOutputHandler(function (module, caption) {
     console.log(module + "() " + caption);
     });
    */
    var initialTrack = app.prepareTrackForMultiplication(factor1, factor2);
    document.getElementById("track").childNodes[0].childNodes[0].innerHTML = getTableDataHtml(initialTrack, 0);
    app.execute();

    machineToWindow(app.extractMachineData(), mode, app.extractResultFromTrack());
}

/**
 * Prepares data to be put on screen
 * @param machineData
 * @param mode
 * @param result
 */
function machineToWindow(machineData, mode, result){
    document.getElementById("outputFrame").style.display = 'block';
    var resultArea = document.getElementById("result");
    resultArea.parentNode.style.display = 'none';
    resultArea.innerHTML = result;
    if(mode == 2){
        var stepButton = document.getElementById("stepButton");
        stepButton.style.display = 'block';
        var counter = 0;
        stepButton.addEventListener("click", function() {
            outMachine(machineData, counter);
            counter++;
            if(counter == machineData.length){
                stepButton.style.display = 'none';
                resultArea.parentNode.style.display = 'block';
            }
        });
    }else if(mode == 1){
        var counter = 0;
        var myInterval = setInterval(function(){
            outMachine(machineData, counter);
            counter++;
            if(counter == machineData.length){
                clearInterval(myInterval);
                resultArea.parentNode.style.display = 'block';
            }
        }, 300);
    }else{
        outMachine(machineData, machineData.length-1);
        resultArea.parentNode.style.display = 'block';
    }
}

/**
 * Puts the data on screen
 * @param machineData
 * @param step
 */
function outMachine(machineData, step){
    var currentStateArea = document.getElementById("currentState");
    var newStateArea = document.getElementById("newState");
    var trackArea = document.getElementById("track").childNodes[0].childNodes[0];
    var stepCountArea = document.getElementById("stepCount");
    var tmpArray = machineData[step];

    currentStateArea.innerHTML = tmpArray[0];
    if(tmpArray[1] == 'q11'){
        newStateArea.innerHTML = "Final State (q11) reached!";
    }else{
        newStateArea.innerHTML = tmpArray[1];
    }
    stepCountArea.innerHTML = tmpArray[4];
    trackArea.innerHTML = getTableDataHtml(tmpArray[2], tmpArray[3]);
}

/**
 * Parses the track of the machine into a table row in html
 * @param toFill {Array} the track
 * @param head current head position
 * @returns {string}
 */
function getTableDataHtml(toFill, head){
    var tableDataHtml = "";
    var insertCount = 0;
    toFill.forEach(function(entry){
        if(insertCount == head){
            if(entry == "x" || entry == '='){
                tableDataHtml = tableDataHtml + '<td class="head bold">' + entry + '</td>';
            }else {
                tableDataHtml = tableDataHtml + '<td class="head">' + entry + '</td>';
            }
        }else{
            if(entry == "x" || entry == '='){
                tableDataHtml = tableDataHtml + '<td class="bold">' + entry + '</td>';
            }else {
                tableDataHtml = tableDataHtml + '<td>' + entry + '</td>';
            }
        }
        insertCount++;
    });
    if(head < 0){
        tableDataHtml = '<td class="head"></td>' + tableDataHtml;
    }
    if(head >= insertCount){
        tableDataHtml = tableDataHtml + '<td class="head"></td>';
    }

    for(var i = 0; i < 15; i++){
        tableDataHtml = "<td></td>" + tableDataHtml + "<td></td>";
    }

    return tableDataHtml
}

/**
 * converts a given number to an unary string
 * @param factorField
 */
function getUnary(factorField){
    var formField = document.getElementsByTagName("input")[factorField];
    var factor = formField.value;
    var unary = "";

    for(var i = 0; i < factor; i++){
        unary = unary + "1";
    }
    formField.value = unary;
}